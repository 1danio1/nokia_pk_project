#include "BtsPort.hpp"
#include "Messages/IncomingMessage.hpp"
#include "Messages/OutgoingMessage.hpp"
#include "Coder/ICoder.hpp"
#include "Coder/CoderFactory.hpp"
#include "Coder/XOREncryption.hpp"
#include "Coder/CaesarEncryption.hpp"
#include <ctime>

namespace ue
{

BtsPort::BtsPort(common::ILogger &logger, common::ITransport &transport, common::PhoneNumber phoneNumber)
    : logger(logger, "[BTS-PORT]"),
      transport(transport),
      phoneNumber(phoneNumber)
{}

void BtsPort::start(IBtsEventsHandler &handler)
{
    transport.registerMessageCallback([this](BinaryMessage msg) {handleMessage(msg);});
    transport.registerDisconnectedCallback([this]() {handleDisconnected();});
    this->handler = &handler;
}

void BtsPort::stop()
{
    transport.registerMessageCallback(nullptr);
    transport.registerDisconnectedCallback(nullptr);
    handler = nullptr;
}

void BtsPort::handleMessage(BinaryMessage msg)
{
    try
    {
        common::IncomingMessage reader{msg};
        auto msgId = reader.readMessageId();
        auto from = reader.readPhoneNumber();
        auto to = reader.readPhoneNumber();

        switch (msgId)
        {
        case common::MessageId::Sib:
        {
            auto btsId = reader.readBtsId();
            handler->handleSib(btsId);
            break;
        }
        case common::MessageId::AttachResponse:
        {
            bool accept = reader.readNumber<std::uint8_t>() != 0u;
            if (accept)
                handler->handleAttachAccept();
            else
                handler->handleAttachReject();
            break;
        }
        case common::MessageId::Sms:
        {
            std::unique_ptr<ICoder> coder(CoderFactory::createCoder(reader));
            if(coder != nullptr)
            {
                std::string text = coder->decodeMessage(reader.readRemainingText());
                handler->handleSmsReceive(from, text);
            }
            else
                logger.logError("Wrong SMS encryption type");
            break;
        }
        case common::MessageId::UnknownRecipient:
        {
            auto failingMsgId = reader.readMessageId();
            auto failingMsgFrom = reader.readPhoneNumber();
            auto failingMsgTo = reader.readPhoneNumber();

            switch (failingMsgId)
            {
            case common::MessageId::Sms:
            {
                logger.logDebug("Unknown recipient of SMS: ", failingMsgTo);
                handler->handleUnknownRecipientOfSms(failingMsgTo);
                break;
            }
            case common::MessageId::CallAccepted:
            case common::MessageId::CallDropped:
            case common::MessageId::CallRequest:
            case common::MessageId::CallTalk:
            {
                logger.logDebug("Unknown recipient of CallAccepted or CallDropped or CallTalk: ", failingMsgTo);
                handler->handleUnknownRecipientOfCall(failingMsgTo);
                break;
            }
            default:
                logger.logError("unknown failing message: ", failingMsgId);
            }
            break;
        }
        case common::MessageId::CallRequest:
        {
            std::shared_ptr<ICoder> coder(CoderFactory::createCoder(reader));
            if(coder != nullptr)
            {
                handler->handleCallRequestReceive(from, coder);
            }
            else
                logger.logError("Wrong Call encryption type");

            break;
        }
        case common::MessageId::CallAccepted:
        {
            std::shared_ptr<ICoder> coder(CoderFactory::createCoder(reader));
            if(coder != nullptr)
            {
                handler->handleCallAcceptReceive(from, coder);
            }
            else
                logger.logError("Wrong Call encryption type");

            break;
        }
        case common::MessageId::CallDropped:
        {
            handler->handleCallDropReceive(from);
            break;
        }
        case common::MessageId::CallTalk:
        {
            std::string text = reader.readRemainingText();
            handler->handleCallTalkReceive(from, text);
            break;
        }
        default:
            logger.logError("unknown message: ", msgId, ", from: ", from);
        }
     }
    catch (std::exception const& ex)
    {
        logger.logError("handleMessage error: ", ex.what());
    }
}


void BtsPort::sendAttachRequest(common::BtsId btsId)
{
    logger.logDebug("sendAttachRequest: ", btsId);
    common::OutgoingMessage msg{common::MessageId::AttachRequest,
                                phoneNumber,
                                common::PhoneNumber{}};
    msg.writeBtsId(btsId);
    transport.sendMessage(msg.getMessage());
}

void BtsPort::sendSms(common::PhoneNumber recipient, const std::string& text)
{
    logger.logDebug("sendSms: ", recipient);
    common::OutgoingMessage msg{common::MessageId::Sms,
                                phoneNumber,
                                recipient};

    srand(time(nullptr));
    EncryptionType encryptionType;
    if(rand() < RAND_MAX/2)
        encryptionType = EncryptionType::XOR;
    else
        encryptionType = EncryptionType::Caesar;
    std::unique_ptr<ICoder> coder(CoderFactory::createCoder(encryptionType));
    if(coder != nullptr)
    {
        coder->addEncryptionInfo(msg);
        msg.writeText(coder->encodeMessage(text));
        transport.sendMessage(msg.getMessage());
    }
    else
        logger.logError("Coder error cannot send SMS");
}

void BtsPort::dropCall(common::PhoneNumber to)
{
    logger.logDebug("dropCall: ", to);
    common::OutgoingMessage msg{common::MessageId::CallDropped,
                                phoneNumber,
                                to};
    transport.sendMessage(msg.getMessage());
}

void BtsPort::acceptCall(common::PhoneNumber to, std::shared_ptr<ICoder> coder)
{
    logger.logDebug("acceptCall: ", to);
    common::OutgoingMessage msg{common::MessageId::CallAccepted,
                                phoneNumber,
                                to};
    coder->addEncryptionInfo(msg);
    transport.sendMessage(msg.getMessage());
}

void BtsPort::handleDisconnected()
{
    logger.logDebug("handleDisconnected");
    handler->handleDisconnected();
}

void BtsPort::sendCallTalk(common::PhoneNumber to, const std::string& text)
{
    logger.logDebug("sendCallTalk: ", to);
    common::OutgoingMessage msg{common::MessageId::CallTalk,
                               phoneNumber,
                               to};
    msg.writeText(text);
    transport.sendMessage(msg.getMessage());
}

void BtsPort::sendCallRequest(common::PhoneNumber to, std::shared_ptr<ICoder> coder)
{
    logger.logDebug("sendCallRequest: ", to);
    common::OutgoingMessage msg{common::MessageId::CallRequest,
                                phoneNumber,
                                to};

    coder->addEncryptionInfo(msg);
    transport.sendMessage(msg.getMessage());
}

}
