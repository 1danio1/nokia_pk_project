#pragma once

#include "IBtsPort.hpp"
#include "Logger/PrefixedLogger.hpp"
#include "ITransport.hpp"

namespace ue
{

class BtsPort : public IBtsPort
{
public:
    BtsPort(common::ILogger& logger, common::ITransport& transport, common::PhoneNumber phoneNumber);
    void start(IBtsEventsHandler& handler);
    void stop();

    void sendAttachRequest(common::BtsId) override;
    void handleDisconnected();
    void sendSms(common::PhoneNumber, const std::string&) override;
    void sendCallRequest(common::PhoneNumber, std::shared_ptr<ICoder> coder) override;
    void dropCall(common::PhoneNumber to) override;
    void acceptCall(common::PhoneNumber, std::shared_ptr<ICoder> coder) override;
    void sendCallTalk(common::PhoneNumber, const std::string&) override;

private:
    void handleMessage(BinaryMessage msg);

    common::PrefixedLogger logger;
    common::ITransport& transport;
    common::PhoneNumber phoneNumber;

    IBtsEventsHandler* handler = nullptr;
};

}
