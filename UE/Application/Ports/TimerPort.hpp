#pragma once

#include "ITimerPort.hpp"
#include "Logger/PrefixedLogger.hpp"
#include "Timer/Timer.hpp"
#include <array>
#include <condition_variable>
#include <mutex>
#include <thread>

namespace ue
{

class TimerPort : public ITimerPort
{
public:
    TimerPort(common::ILogger& logger);

    void start(ITimerEventsHandler& handler);
    void stop();

    // ITimerPort interface
#define TIMER_START_STOP_FUNCTION_DECLARATION(X)    \
    void start##X##Timer() override;                \
    void stop##X##Timer() override;

    FOR_ALL_TIMERS(TIMER_START_STOP_FUNCTION_DECLARATION)

#undef TIMER_START_STOP_FUNCTION_DECLARATION

private:
    common::PrefixedLogger logger;
    ITimerEventsHandler* handler = nullptr;

#define TIMER_ENUM_ENTRY(X) X,
    enum TimersEnum {
        FOR_ALL_TIMERS(TIMER_ENUM_ENTRY)
        TIMERCOUNT
    };
#undef TIMER_ENUM_ENTRY

    std::array<Timer, TIMERCOUNT> timers;
    std::thread timerThread;
    std::mutex timerMutex;
    std::condition_variable timerCV;
    bool shouldRun;

    Timer::TimePoint getEarliestTimerEnd();
    bool anyRunningTimers();
    void timerThreadFunc();
};

}
