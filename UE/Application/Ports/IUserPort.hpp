#pragma once
#include "Messages/PhoneNumber.hpp"
#include "Sms/Sms.hpp"
#include "Coder/ICoder.hpp"
#include <memory>

namespace ue
{

class IUserEventsHandler
{
public:
    virtual ~IUserEventsHandler() = default;
    virtual void handleSmsSend(common::PhoneNumber, const std::string&) = 0;
    virtual void handleCallRequestSend(common::PhoneNumber, std::shared_ptr<ICoder>) = 0;
    virtual void handleIncomingCallDrop() = 0;
    virtual void handleIncomingCallAccept(std::shared_ptr<ICoder> encryptor) = 0;
    virtual void handleCancelUnknownRecipientShow() = 0;
    virtual void handleOutgoingCallDrop() = 0;
    virtual void handleUserEndCall() = 0;
    virtual void handleCallTalkSend(const std::string&) = 0;
    virtual void handleStateClose() = 0;
    virtual void handleDialing() = 0;
    virtual void handleViewSms(const Sms& sms) = 0;
    virtual void handleViewSmsList() = 0;
    virtual void handleSmsCompose() = 0;
    virtual void handleUEClose() = 0;
};

class IUserPort
{
public:
    virtual ~IUserPort() = default;

    virtual void showNotConnected() = 0;
    virtual void showConnecting() = 0;
    virtual void showConnected() = 0;
    virtual void showMainMenu() = 0;
    virtual void showSmsNotification() = 0;
    virtual void markSmsAsFailed(common::PhoneNumber) = 0;
    virtual void addReceivedSms(common::PhoneNumber, const std::string&) = 0;
    virtual void showCallMode(common::PhoneNumber) = 0;
    virtual void showIncomingCall(common::PhoneNumber) = 0;
    virtual void showOutgoingCall(common::PhoneNumber) = 0;
    virtual void showUnknownRecipientOfCall(common::PhoneNumber) = 0;
    virtual void showIncomingCallTalk(common::PhoneNumber, const std::string&) = 0;
    virtual void showSmsComposeMode() = 0;
    virtual void showSmsList() = 0;
    virtual void showDialMode() = 0;
    virtual void showSms(const Sms& sms) = 0;
};

}
