#include "SmsDb.hpp"

namespace ue
{

SmsDb::SmsDb(common::ILogger& logger)
: logger(logger, "[SmsDb]")
{}

void SmsDb::addReceivedSms(PhoneNumber sender, PhoneNumber recipient, const std::string& text)
{
    receivedMessages.emplace_back(Sms{sender, recipient, text, Status::notRead});
    logger.logDebug("Added received sms from: ", sender);
}

void SmsDb::addSentSms(PhoneNumber sender, PhoneNumber recipient, const std::string& text)
{
    sentMessages.emplace_back(Sms{sender, recipient, text, Status::sent});
    logger.logDebug("Added sent sms to: ", recipient);
}

std::vector<Sms>& SmsDb::getReceivedMessages()
{
    return receivedMessages;
}

std::vector<Sms>& SmsDb::getSentMessages()
{
    return sentMessages;
}


}
