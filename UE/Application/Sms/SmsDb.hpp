#pragma once

#include "ISmsDb.hpp"
#include "Logger/PrefixedLogger.hpp"

namespace ue
{
    class SmsDb : public ISmsDb
    {
    public:
        SmsDb(common::ILogger& logger);

        void addReceivedSms(PhoneNumber sender,
                            PhoneNumber recipient,
                            const std::string& text) override;

        void addSentSms(PhoneNumber sender,
                        PhoneNumber recipient,
                        const std::string& text) override;

        std::vector<Sms>& getReceivedMessages() override;
        std::vector<Sms>& getSentMessages() override;

    private:
        common::PrefixedLogger logger;
        std::vector<Sms> sentMessages;
        std::vector<Sms> receivedMessages;
    };

}
