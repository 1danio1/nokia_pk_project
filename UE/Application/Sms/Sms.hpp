#pragma once

#include <string>
#include <chrono>
#include "Messages.hpp"

namespace ue
{
enum class Status {read, notRead, sent, notReceived};

    struct Sms
    {
        common::PhoneNumber from;
        common::PhoneNumber to;
        std::string text;
        Status status;
        std::chrono::system_clock::time_point timestamp = std::chrono::system_clock::now();
    };

}
