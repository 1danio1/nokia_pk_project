#include "SmsListViewState.hpp"
#include "MainMenuState.hpp"
#include "SmsViewState.hpp"

namespace ue
{

SmsListViewState::SmsListViewState(Context &context)
    : ConnectedState(context, "SmsListViewState")
{
    context.user.showSmsList();
}

void SmsListViewState::handleStateClose()
{
    logger.logDebug("handleStateClose");
    context.setState<MainMenuState>();
}

void SmsListViewState::handleViewSms(const Sms& sms)
{
    logger.logDebug("handleViewSms");
    context.setState<SmsViewState>(sms);
}

void SmsListViewState::handleSmsReceive(common::PhoneNumber from, const std::string& text)
{
    logger.logDebug("handleSmsReceive from: ", from);
    context.user.addReceivedSms(from, text);
    context.user.showSmsNotification();
    context.user.showSmsList();
}

}
