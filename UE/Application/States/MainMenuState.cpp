#include "MainMenuState.hpp"
#include "DialingState.hpp"
#include "SmsListViewState.hpp"
#include "SmsComposeState.hpp"

namespace ue
{

MainMenuState::MainMenuState(Context &context)
    : ConnectedState(context, "MainMenuState")
{
    context.user.showMainMenu();
}

void MainMenuState::handleDialing()
{
    logger.logDebug("handleDialing");
    context.setState<DialingState>();
}

void MainMenuState::handleViewSmsList()
{
    logger.logDebug("handleViewSmsList");
    context.setState<SmsListViewState>();
}

void MainMenuState::handleSmsCompose()
{
    logger.logDebug("handleSmsCompose");
    context.setState<SmsComposeState>();
}


}
