#pragma once

#include "BaseState.hpp"

namespace ue
{

class ConnectingState : public BaseState
{
public:
    ConnectingState(Context& context, common::BtsId btsId);

    //IBtsEventHandler
    void handleAttachAccept() override;
    void handleAttachReject() override;
    void handleAttachTimeout() override;
    void handleDisconnected() override;
    void handleUEClose() override;
    void handleSib(common::BtsId btsId) override;
};

}
