#include "OutgoingCallState.hpp"
#include "MainMenuState.hpp"
#include "TalkingState.hpp"
#include "IncomingCallState.hpp"

namespace ue
{

OutgoingCallState::OutgoingCallState(Context &context, common::PhoneNumber to, std::shared_ptr<ICoder> encryptor)
    : ConnectedState(context, "OutgoingCallState"), calledNumber{to}, encryptor{encryptor}
{
    context.timer.startOutgoingCallTimer();
    context.bts.sendCallRequest(to, encryptor);
    context.user.showOutgoingCall(calledNumber);
}

void OutgoingCallState::handleStateClose()
{
    logger.logDebug("handleStateClose");
    context.timer.stopOutgoingCallTimer();
    context.bts.dropCall(calledNumber);
    context.setState<MainMenuState>();
}

void OutgoingCallState::handleOutgoingCallTimeout()
{
    logger.logDebug("handleOutgoingCallTimeout");
    context.bts.dropCall(calledNumber);
    context.setState<MainMenuState>();
}

void OutgoingCallState::handleCallAcceptReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor)
{
    logger.logDebug("handleCallAccept from: ", from);
    if(from == calledNumber)
    {
        context.timer.stopOutgoingCallTimer();
        context.setState<TalkingState>(calledNumber, decryptor, encryptor);
    }
    else logger.logError("Received callAccept from different number!");
}

void OutgoingCallState::handleCallDropReceive(common::PhoneNumber from)
{
    logger.logDebug("handleCallDrop from: ", from);
    if(from == calledNumber)
    {
        context.timer.stopOutgoingCallTimer();
        context.setState<MainMenuState>();
    }
    else logger.logError("Received callDrop from different number!");
}

void OutgoingCallState::handleUEClose()
{
    context.timer.stopOutgoingCallTimer();
    context.bts.dropCall(calledNumber);
}

void OutgoingCallState::handleCallRequestReceive(common::PhoneNumber caller, std::shared_ptr<ICoder> decryptor)
{
    if(caller!=calledNumber){

    context.timer.stopOutgoingCallTimer();
    logger.logDebug("handleReceivingCallRequest");
    context.bts.dropCall(calledNumber);
    context.setState<IncomingCallState>(caller, decryptor, false);

    }
}
}
