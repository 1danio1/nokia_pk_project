#pragma once

#include "ConnectedState.hpp"

namespace ue
{

class OutgoingCallState : public ConnectedState
{
public:
    OutgoingCallState(Context& context, common::PhoneNumber to, std::shared_ptr<ICoder> encryptor);

    //IUserEventHandler
    void handleStateClose() override;
    void handleCallAcceptReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor) override;
    void handleUEClose() override;

    //IBtsEventHandler
    void handleCallDropReceive(common::PhoneNumber from) override;

    //ITimerEventHandler
    void handleOutgoingCallTimeout() override;
    void handleCallRequestReceive(common::PhoneNumber caller, std::shared_ptr<ICoder> decryptor) override;

private:
    common::PhoneNumber calledNumber;
    std::shared_ptr<ICoder> encryptor;
};

}
