#pragma once

#include "ConnectedState.hpp"

namespace ue
{

class SmsListViewState : public ConnectedState
{
public:
    SmsListViewState(Context& context);

    //IUserEventHandler
    void handleStateClose() override;
    void handleViewSms(const Sms& sms) override;
    void handleSmsReceive(common::PhoneNumber from, const std::string& text) override;
};

}
