#include "BaseState.hpp"

namespace ue
{

BaseState::BaseState(Context &context, const std::string &name)
    : context(context),
      logger(context.logger, "[" + name + "]")
{
    logger.logDebug("entry");
}

BaseState::~BaseState()
{
    logger.logDebug("exit");
}

void BaseState::handleAttachTimeout()
{
    logger.logError("Uexpected: handleAttachTimeout");
}

void BaseState::handleIncomingCallTimeout()
{
    logger.logError("Uexpected: handleIncomingCallTimeout");
}

void BaseState::handleOutgoingCallTimeout()
{
    logger.logError("Uexpected: handleOutgoingCallTimeout");
}

void BaseState::handleUnknownRecipientTimeout()
{
    logger.logError("Uexpected: handleUnknownRecipientTimeout");
}

void BaseState::handleSib(common::BtsId btsId)
{
    logger.logError("Uexpected: handleSib: ", btsId);
}

void BaseState::handleAttachAccept()
{
    logger.logError("Uexpected: handleTimeout");
}

void BaseState::handleAttachReject()
{
    logger.logError("Uexpected: handleAttachReject");
}

void BaseState::handleDisconnected(){
    logger.logError("Unexpected: handleDisconnected");
}

void BaseState::handleSmsReceive(common::PhoneNumber from, const std::string& text)
{
    logger.logError("Unexpected: handleSmsReceive: ", from, "  ", text);
}

void BaseState::handleSmsSend(common::PhoneNumber to, const std::string& text)
{
    logger.logError("Unexpected: handleSmsSend: ", to, "  ", text);
}

void BaseState::handleUnknownRecipientOfSms(common::PhoneNumber to)
{
    logger.logError("Unexpected: handleUnknownRecipientOfSms: ", to);
}

void BaseState::handleCallAcceptReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor)
{
    logger.logError("Unexpected: handleCallAccept", from);
}

void BaseState::handleCallDropReceive(common::PhoneNumber from)
{
    logger.logError("Unexpected: handleCallDrop", from);
}

void BaseState::handleCallRequestSend(common::PhoneNumber to, std::shared_ptr<ICoder> encryptor)
{
    logger.logError("Unexpected:: handleCallRequestSend: ", to);
}

void BaseState::handleCallRequestReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor)
{
    logger.logError("Unexpected: handleCallRequest: ", from);
}

void BaseState::handleIncomingCallDrop()
{
    logger.logError("Unexpected: handleIncomingCallDrop: ");
}

void BaseState::handleIncomingCallAccept(std::shared_ptr<ICoder> encryptor)
{
    logger.logError("Unexpected: handleIncomingCallAccept: ");
}

void BaseState::handleUnknownRecipientOfCall(common::PhoneNumber to)
{
    logger.logError("Unexpected: handleUnknownRecipientOfCall: ", to);
}

void BaseState::handleCancelUnknownRecipientShow()
{
    logger.logError("Unexpected: handleCancelUnknownRecipientShow");
}

void BaseState::handleOutgoingCallDrop()
{
    logger.logError("Unexpected: handleOutgoingCallDrop");
}

void BaseState::handleUserEndCall()
{
    logger.logError("Unexpected: handleUserEndCall");
}

void BaseState::handleCallTalkSend(const std::string& text)
{
    logger.logError("Unexpected: handleCallTalkSend");
}

void BaseState::handleTalkingTimeout()
{
    logger.logError("Unexpected: handleTalkingTimeout: ");
}

void BaseState::handleCallTalkReceive(common::PhoneNumber from, const std::string& text)
{
    logger.logError("Unexpected: handleCallTalkReceive: ", from);
}

void BaseState::handleStateClose()
{
    logger.logError("Unexpected: handleStateClose");
}

void BaseState::handleDialing()
{
    logger.logError("Unexpected: handleDialing");
}

void BaseState::handleViewSms(const Sms& sms)
{
    logger.logError("Unexpected: handleViewSms");
}

void BaseState::handleViewSmsList()
{
    logger.logError("Unexpected: handleViewSmsList");
}

void BaseState::handleSmsCompose()
{
    logger.logError("Unexpected: handleSmsCompose");
}

void BaseState::handleUEClose()
{
    logger.logError("Unexpected: handleUEClose");
}

}
