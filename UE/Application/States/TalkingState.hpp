#pragma once

#include "ConnectedState.hpp"

namespace ue
{

class TalkingState : public ConnectedState
{
public:
    TalkingState(Context& context, common::PhoneNumber partnerNumber, std::shared_ptr<ICoder> decryptor, std::shared_ptr<ICoder> encryptor);

    // IUserEventsHandler interface
    void handleCancelUnknownRecipientShow() override;
    void handleUserEndCall() override;
    void handleCallTalkSend(const std::string& text) override;
    void handleStateClose() override;
    void handleUEClose() override;

    // IBtsEventsHandler interface
    void handleUnknownRecipientOfCall(common::PhoneNumber to) override;
    void handleCallDropReceive(common::PhoneNumber from) override;
    void handleCallTalkReceive(common::PhoneNumber from, const std::string& text) override;
    void handleCallRequestReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor) override;

    // ITimerEventsHandler interface
    void handleUnknownRecipientTimeout() override;
    void handleTalkingTimeout() override;

private:
    common::PhoneNumber partnerNumber;
    std::shared_ptr<ICoder> decryptor;
    std::shared_ptr<ICoder> encryptor;
};

}
