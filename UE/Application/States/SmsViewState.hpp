#pragma once

#include "ConnectedState.hpp"

namespace ue
{

class SmsViewState : public ConnectedState
{
public:
    SmsViewState(Context& context, const Sms& sms);
    void handleStateClose() override;
};

}
