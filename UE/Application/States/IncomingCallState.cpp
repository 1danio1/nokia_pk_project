#include "IncomingCallState.hpp"
#include "MainMenuState.hpp"
#include "TalkingState.hpp"

namespace ue
{

IncomingCallState::IncomingCallState(Context &context, common::PhoneNumber caller, std::shared_ptr<ICoder> decryptor, bool shouldComeBackToPrevState)
    : ConnectedState(context, "IncomingCallState"), caller{caller}, decryptor{decryptor}, shouldComeBackToPrevState{shouldComeBackToPrevState}
{
    context.user.showIncomingCall(caller);
    context.timer.startIncomingCallTimer();
}

void IncomingCallState::handleStateClose()
{
    logger.logDebug("handleStateClose");
    context.timer.stopIncomingCallTimer();
    context.bts.dropCall(caller);
    if(shouldComeBackToPrevState) context.setPrevState();
    else context.setState<MainMenuState>();
}

void IncomingCallState::handleIncomingCallTimeout()
{
    logger.logDebug("handleIncomingCallTimeout");
    context.bts.dropCall(caller);

    if(shouldComeBackToPrevState) context.setPrevState();
    else context.setState<MainMenuState>();
}

void IncomingCallState::handleIncomingCallAccept(std::shared_ptr<ICoder> encryptor)
{
    logger.logDebug("handleIncomingCallAccept");
    context.timer.stopIncomingCallTimer();
    context.bts.acceptCall(caller, encryptor);
    context.setState<TalkingState>(caller, decryptor, encryptor);
}

void IncomingCallState::handleCallDropReceive(common::PhoneNumber from)
{
    logger.logDebug("handleCallDropReceive from: ", from);
    if(caller == from){
        context.timer.stopIncomingCallTimer();
        if(shouldComeBackToPrevState)
        context.setPrevState();
        else context.setState<MainMenuState>();
    }
    else logger.logError("Received callDrop from different number!");
}

void IncomingCallState::handleUEClose()
{
    logger.logDebug("handleUEClose");
    context.timer.stopIncomingCallTimer();
    context.bts.dropCall(caller);
}

void IncomingCallState::handleCallRequestReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor)
{
    logger.logDebug("handleCallRequestReceive from: ", from);
    if(from != caller)
    {
        context.bts.dropCall(from);
    }
}



}
