#pragma once

#include "BaseState.hpp"

namespace ue
{

class ConnectedState : public BaseState
{
public:
    ConnectedState(Context& context);
    void handleUnknownRecipientTimeout() override;
    void handleDisconnected() override;
    void handleSmsReceive(common::PhoneNumber from, const std::string& text) override;
    void handleUnknownRecipientOfSms(common::PhoneNumber to) override;
    void handleCallRequestReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor) override;
    void handleUnknownRecipientOfCall(common::PhoneNumber to) override;
    void handleUEClose() override;

protected:
    ConnectedState(Context& context, const std::string& name);
};

}
