#pragma once

#include "ConnectedState.hpp"

namespace ue
{

class SmsComposeState : public ConnectedState
{
public:
    SmsComposeState(Context& context);

    //IUserEventHandler
    void handleStateClose() override;
    void handleSmsSend(common::PhoneNumber to, const std::string& text) override;
};

}
