#pragma once

#include "ConnectedState.hpp"

namespace ue
{

class IncomingCallState : public ConnectedState
{
public:
    IncomingCallState(Context& context, common::PhoneNumber caller, std::shared_ptr<ICoder> decryptor, bool shouldComeBackToPrevState = 1);

    //IUserEventHandler
    void handleStateClose() override;
    void handleIncomingCallAccept(std::shared_ptr<ICoder> encryptor) override;
    void handleUEClose() override;

    //IBtsEventHandler
    void handleCallDropReceive(common::PhoneNumber from) override;
    void handleCallRequestReceive(common::PhoneNumber from, std::shared_ptr<ICoder> decryptor) override;

    //ITimerEventHandler
    void handleIncomingCallTimeout() override;

private:
    common::PhoneNumber caller;
    std::shared_ptr<ICoder> decryptor;
    bool shouldComeBackToPrevState;
};

}
