#pragma once
#include "ICoder.hpp"

namespace ue {

class CaesarEncryption :public ICoder
{
public:
     CaesarEncryption(std::uint8_t key);
     std::string encodeMessage(std::string text) override;
     std::string decodeMessage(std::string text) override;
     void addEncryptionInfo(common::OutgoingMessage &msg) override;

private:
     std::string caesarCode(std::string text, uint8_t key);
     std::uint8_t encryptionKey;



};

}
