#include "CaesarEncryption.hpp"

namespace ue {
CaesarEncryption::CaesarEncryption(std::uint8_t key)
    :encryptionKey(key)
{}

std::string CaesarEncryption::caesarCode(std::string text, std::uint8_t key)
{
    for (char& c : text)
    {
        c = static_cast<char>(c + key);
    }
    return text;
}

std::string CaesarEncryption::encodeMessage(std::string text)
{
    return caesarCode(text, encryptionKey);
}

std::string CaesarEncryption::decodeMessage(std::string text)
{
    return caesarCode(text, -encryptionKey);
}

void CaesarEncryption::addEncryptionInfo(common::OutgoingMessage &msg)
{
    msg.writeNumber<std::uint8_t>(static_cast<std::uint8_t>(EncryptionType::Caesar));
    msg.writeNumber<std::uint8_t>(encryptionKey);
}

}
