#pragma once

#include <string>
#include "Messages/OutgoingMessage.hpp"

namespace ue
{

enum class EncryptionType
{
    None = 0,
    XOR = 1,
    Caesar = 2,
    RSA = 3
};

class ICoder
{
public:
    virtual ~ICoder() = default;

    virtual std::string encodeMessage(std::string text) = 0;
    virtual std::string decodeMessage(std::string text) = 0;
    virtual void addEncryptionInfo(common::OutgoingMessage &msg) = 0;

};

}
