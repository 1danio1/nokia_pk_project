#include "CoderFactory.hpp"
#include <ctime>

namespace ue
{

ICoder* CoderFactory::createCoder(EncryptionType type)
{
    srand(time(nullptr));
    ICoder* tmp = nullptr;
    switch(type)
    {
    case EncryptionType::XOR:
        tmp = new XOREncryption(rand()%255);
        break;
    case EncryptionType::Caesar:
        tmp = new CaesarEncryption(rand()%255);
        break;
//    case EncryptionType::RSA:

//        break;
    case EncryptionType::None:
        tmp = new NoCoder();
        break;
    default:
        tmp = nullptr;
        break;
    }
    return tmp;
}

ICoder* CoderFactory::createCoder(common::IncomingMessage &msg)
{
    ICoder* tmp = nullptr;
    auto encryptionType = msg.readNumber<std::uint8_t>();
    switch(static_cast<EncryptionType>(encryptionType))
    {
    case EncryptionType::XOR:
        tmp = new XOREncryption(msg.readNumber<std::uint8_t>());
        break;
    case EncryptionType::Caesar:
        tmp = new CaesarEncryption(msg.readNumber<std::uint8_t>());
        break;
//    case EncryptionType::RSA:

//        break;
    case EncryptionType::None:
        tmp = new NoCoder();
        break;
    default:
        tmp = nullptr;
        break;
    }
    return tmp;
}

}
