#pragma once

#include "ICoder.hpp"

namespace ue
{

class NoCoder : public ICoder
{
public:
    NoCoder();

    std::string encodeMessage(std::string text) override;
    std::string decodeMessage(std::string text) override;
    void addEncryptionInfo(common::OutgoingMessage &msg) override;
};

}
