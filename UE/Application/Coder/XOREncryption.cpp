#include "XOREncryption.hpp"

namespace ue
{
XOREncryption::XOREncryption(std::uint8_t key)
    :   encryptionKey(key)
{}

std::string XOREncryption::XORCode(std::string text)
{
    for(char& c :text)
    {
        c = static_cast<char>( c ^ encryptionKey );
    }

    return text;
}

std::string XOREncryption::encodeMessage(std::string text)
{
   return XORCode(text);
}

std::string XOREncryption::decodeMessage(std::string text)
{
    return XORCode(text);
}

void XOREncryption::addEncryptionInfo(common::OutgoingMessage &msg)
{
    msg.writeNumber<std::uint8_t>(static_cast<std::uint8_t>(EncryptionType::XOR));
    msg.writeNumber<std::uint8_t>(encryptionKey);
}

}
