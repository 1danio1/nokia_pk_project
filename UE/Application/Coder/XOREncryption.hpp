#pragma once

#include "ICoder.hpp"

namespace ue
{

class XOREncryption :public ICoder
{
public:
    XOREncryption(std::uint8_t key);

    std::string encodeMessage(std::string text) override;
    std::string decodeMessage(std::string text) override;
    void addEncryptionInfo(common::OutgoingMessage &msg) override;

private:
    std::uint8_t encryptionKey;
    std::string XORCode(std::string text);

};

}
