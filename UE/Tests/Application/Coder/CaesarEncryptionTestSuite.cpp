#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <vector>

#include "Coder/CaesarEncryption.hpp"

namespace ue
{
using namespace ::testing;

class CaesarEncryptionTestSuite : public Test
{
protected:
    const std::uint8_t CAESAR_KEY = 128;
    const std::string TEXT = "ABc @1";
    const std::vector<unsigned char> ENCODED_SIGN {
                static_cast<unsigned char>('A' + CAESAR_KEY),
                static_cast<unsigned char>('B' + CAESAR_KEY),
                static_cast<unsigned char>('c' + CAESAR_KEY),
                static_cast<unsigned char>(' ' + CAESAR_KEY),
                static_cast<unsigned char>('@' + CAESAR_KEY),
                static_cast<unsigned char>('1' + CAESAR_KEY)
    };
    const std::string ENCODED_TEXT{ENCODED_SIGN.begin(), ENCODED_SIGN.end()};
    CaesarEncryption objectUnderTest{CAESAR_KEY};

    const std::string OTHER_TEXT= "ABc @!# ążźćśóła z ()%";
public:


    CaesarEncryptionTestSuite(){}
    ~CaesarEncryptionTestSuite(){}
};


TEST_F(CaesarEncryptionTestSuite, shallEncodeMessage)
{
    ASSERT_EQ(ENCODED_TEXT, objectUnderTest.encodeMessage(TEXT));
}

TEST_F(CaesarEncryptionTestSuite, shallDecodeMessage)
{
    ASSERT_EQ(TEXT, objectUnderTest.decodeMessage(ENCODED_TEXT));
}

TEST_F(CaesarEncryptionTestSuite, shallEncodeAndDecodeMessage)
{
    const std::string ENCODED_OTHER_TEXT = objectUnderTest.encodeMessage(OTHER_TEXT);
    ASSERT_EQ(OTHER_TEXT, objectUnderTest.decodeMessage(ENCODED_OTHER_TEXT));
}
}
