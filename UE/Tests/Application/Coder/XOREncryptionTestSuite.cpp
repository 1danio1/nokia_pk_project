#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <vector>

#include "Coder/XOREncryption.hpp"

namespace ue
{
using namespace ::testing;

class XOREncryptionTestSuite : public Test
{
protected:
    const std::uint8_t XOR_KEY = 128;
    const std::string TEXT = "h E$.5";
    const std::vector<unsigned char> ENCODED_SIGN {
                static_cast<unsigned char>('h' ^ XOR_KEY),
                static_cast<unsigned char>(' ' ^ XOR_KEY),
                static_cast<unsigned char>('E' ^ XOR_KEY),
                static_cast<unsigned char>('$' ^ XOR_KEY),
                static_cast<unsigned char>('.' ^ XOR_KEY),
                static_cast<unsigned char>('5' ^ XOR_KEY)
    };
    const std::string ENCODED_TEXT{ENCODED_SIGN.begin(), ENCODED_SIGN.end()};
    XOREncryption objectUnderTest{XOR_KEY};

    const std::string OTHER_TEXT= "ABc @!# ążźćśóła z ()%";
public:


    XOREncryptionTestSuite(){}
    ~XOREncryptionTestSuite(){}
};


TEST_F(XOREncryptionTestSuite, shallEncodeMessage)
{
    ASSERT_EQ(ENCODED_TEXT, objectUnderTest.encodeMessage(TEXT));
}

TEST_F(XOREncryptionTestSuite, shallDecodeMessage)
{
    ASSERT_EQ(TEXT, objectUnderTest.decodeMessage(ENCODED_TEXT));
}

TEST_F(XOREncryptionTestSuite, shallEncodeAndDecodeMessage)
{
    const std::string ENCODED_OTHER_TEXT = objectUnderTest.encodeMessage(OTHER_TEXT);
    ASSERT_EQ(OTHER_TEXT, objectUnderTest.decodeMessage(ENCODED_OTHER_TEXT));
}
}
