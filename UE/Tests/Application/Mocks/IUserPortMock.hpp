#pragma once

#include <gmock/gmock.h>
#include "Ports/IUserPort.hpp"
#include "Sms/Sms.hpp"

namespace ue
{

class IUserEventsHandlerMock : public IUserEventsHandler
{
public:
    IUserEventsHandlerMock();
    ~IUserEventsHandlerMock() override;
    MOCK_METHOD2(handleSmsSend, void(common::PhoneNumber, const std::string&));
    MOCK_METHOD2(handleCallRequestSend, void(common::PhoneNumber, std::shared_ptr<ICoder>));
    MOCK_METHOD0(handleIncomingCallDrop, void());
    MOCK_METHOD1(handleIncomingCallAccept, void(std::shared_ptr<ICoder>));
    MOCK_METHOD0(handleCancelUnknownRecipientShow, void());
    MOCK_METHOD0(handleOutgoingCallDrop, void());
    MOCK_METHOD0(handleUserEndCall, void());
    MOCK_METHOD1(handleCallTalkSend, void(const std::string&));
    MOCK_METHOD0(handleStateClose, void());
    MOCK_METHOD0(handleDialing, void());
    MOCK_METHOD1(handleViewSms, void(const Sms& sms));
    MOCK_METHOD0(handleViewSmsList, void());
    MOCK_METHOD0(handleSmsCompose, void());
    MOCK_METHOD0(handleUEClose, void());
};

class IUserPortMock : public IUserPort
{
public:
    IUserPortMock();
    ~IUserPortMock() override;

    MOCK_METHOD0(showNotConnected, void());
    MOCK_METHOD0(showConnecting, void());
    MOCK_METHOD0(showConnected, void());
    MOCK_METHOD0(showMainMenu, void());
    MOCK_METHOD0(showSmsNotification, void());
    MOCK_METHOD1(markSmsAsFailed, void(common::PhoneNumber));
    MOCK_METHOD2(addReceivedSms, void(common::PhoneNumber, const std::string&));
    MOCK_METHOD1(showCallMode, void(common::PhoneNumber));
    MOCK_METHOD1(showIncomingCall, void(common::PhoneNumber));
    MOCK_METHOD1(showOutgoingCall, void(common::PhoneNumber));
    MOCK_METHOD1(showUnknownRecipientOfCall, void(common::PhoneNumber));
    MOCK_METHOD2(showIncomingCallTalk, void(common::PhoneNumber, const std::string&));
    MOCK_METHOD0(showSmsComposeMode,void());
    MOCK_METHOD0(showSmsList,void());
    MOCK_METHOD0(showDialMode,void());
    MOCK_METHOD1(showSms, void(const Sms& sms));
};

}
