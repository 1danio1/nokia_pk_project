#pragma once

#include <gmock/gmock.h>
#include "Ports/IBtsPort.hpp"

namespace ue
{

class IBtsEventsHandlerMock : public IBtsEventsHandler
{
public:
    IBtsEventsHandlerMock();
    ~IBtsEventsHandlerMock() override;

    MOCK_METHOD1(handleSib, void(common::BtsId));
    MOCK_METHOD0(handleAttachAccept, void());
    MOCK_METHOD0(handleAttachReject, void());
    MOCK_METHOD0(handleDisconnected, void());
    MOCK_METHOD2(handleSmsReceive, void(common::PhoneNumber, const std::string&));
    MOCK_METHOD1(handleUnknownRecipientOfSms, void(common::PhoneNumber));
    MOCK_METHOD2(handleCallAcceptReceive, void(common::PhoneNumber, std::shared_ptr<ICoder>));
    MOCK_METHOD1(handleCallDropReceive, void(common::PhoneNumber));
    MOCK_METHOD2(handleCallRequestReceive, void(common::PhoneNumber, std::shared_ptr<ICoder>));
    MOCK_METHOD1(handleUnknownRecipientOfCall, void(common::PhoneNumber));
    MOCK_METHOD2(handleCallTalkReceive, void(common::PhoneNumber, const std::string&));

};

class IBtsPortMock : public IBtsPort
{
public:
    IBtsPortMock();
    ~IBtsPortMock() override;

    MOCK_METHOD1(sendAttachRequest, void(common::BtsId));
    MOCK_METHOD0(handleDisconnected, void());
    MOCK_METHOD2(sendSms, void(common::PhoneNumber, const std::string&));
    MOCK_METHOD2(sendCallRequest, void(common::PhoneNumber, std::shared_ptr<ICoder>));
    MOCK_METHOD1(dropCall, void(common::PhoneNumber));
    MOCK_METHOD2(acceptCall, void(common::PhoneNumber, std::shared_ptr<ICoder>));
    MOCK_METHOD2(sendCallTalk, void(common::PhoneNumber, const std::string&));

};

}
