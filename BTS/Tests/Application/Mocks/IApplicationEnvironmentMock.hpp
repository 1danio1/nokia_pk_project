#pragma once

#include <gmock/gmock.h>
#include "IApplicationEnvironment.hpp"

namespace bts
{

class IApplicationEnvironmentMock : public IApplicationEnvironment
{
public:
    IApplicationEnvironmentMock();
    ~IApplicationEnvironmentMock() override;

    MOCK_METHOD0(getConsole, IConsole&());
    MOCK_METHOD1(registerUeConnectedCallback, void(UeConnectedCallback));
    MOCK_METHOD0(getLogger, ILogger&());
    MOCK_CONST_METHOD0(getBtsId, BtsId());
    MOCK_CONST_METHOD0(getAddress, std::string());
    MOCK_METHOD0(startMessageLoop, void());
};


}
